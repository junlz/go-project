package main

import (
	"fmt"
	"time"
)

func main()  {
	data := make(map[int]int,10)
	for i:=1;i<=10; i++ {
		data[i] = i
	}
	//for key,value := range data {
	//	go func() {
	//		fmt.Println("k->",key,"v->",value)
	//	}()
	//}
	//出现数据某个数字打印多次  goruntine栈上的变量没有刷新
	for key,value := range data {
		go func(key,value int) {
			fmt.Println("k->",key,"v->",value)
		}(key,value)
	}
	time.Sleep(time.Second * 5)
}
