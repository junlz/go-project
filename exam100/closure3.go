package main

import "fmt"
//闭包例子2
func func3() int {
	val := 10
	defer func() {
		val += 1
	}()

	return val
}

func printFunc3()  {
	fmt.Println(func3())
}

func main()  {
	printFunc3()
}
