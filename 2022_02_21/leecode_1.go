package main

import "fmt"

//输入：nums = [3,3], target = 6
//输出：[0,1]

func twoSum(nums []int,target int) []int {
	numIndex := make(map[int]int , len(nums))
	for i, num := range nums {
		pair := target - num
		if j,ok := numIndex[pair]; ok && i != j {
			return []int{i,j}
		}
		numIndex[num] = i
	}
	return nil
}

func main()  {
	nums1 := []int{3,5,9}
	target := 14
	fmt.Println(twoSum(nums1,target))
}