package main

import (
	"fmt"
	"os"
	"reflect"
	"strings"
)

//接下来呢，我们利用反射实现一个简单的功能，来看看反射如何帮助我们简化代码的。
//
//假设有一个配置类 Config，每个字段是一个配置项。为了简化实现，假设字段均为 string 类型：
type Config struct {
	Name    string `json:"server-name"`
	IP      string `json:"server-ip"`
	URL     string `json:"server-url"`
	Timeout string `json:"timeout"`
}
//生成结果
//type Config struct {
//	Name    string `json:"server-name"` // CONFIG_SERVER_NAME
//	IP      string `json:"server-ip"`   // CONFIG_SERVER_IP
//	URL     string `json:"server-url"`  // CONFIG_SERVER_URL
//	Timeout string `json:"timeout"`     // CONFIG_TIMEOUT
//}

func readConfig() *Config {
	// read from xxx.json，省略
	config := Config{}
	typ := reflect.TypeOf(config)
	value := reflect.Indirect(reflect.ValueOf(&config))
	for i := 0; i < typ.NumField(); i++ {
		f := typ.Field(i)
		if v, ok := f.Tag.Lookup("json"); ok {
			key := fmt.Sprintf("CONFIG_%s", strings.ReplaceAll(strings.ToUpper(v), "-", "_"))
			if env, exist := os.LookupEnv(key); exist {
				value.FieldByName(f.Name).Set(reflect.ValueOf(env))
			}
		}
	}
	return &config
}

func main() {
	os.Setenv("CONFIG_SERVER_NAME", "global_server")
	os.Setenv("CONFIG_SERVER_IP", "10.0.0.1")
	os.Setenv("CONFIG_SERVER_URL", "geektutu.com")
	c := readConfig()
	fmt.Printf("%+v", c)
}
