package main

import (
	"fmt"
	"unsafe"
)

type Args struct {
	num1 int
	num2 int
}
type Flag struct {
	num1 int16
	num2 int32
}

//在 Go 语言中，我们可以使用 unsafe.Sizeof 计算出一个数据类型实例需要占用的字节数
func main()  {
	fmt.Println(unsafe.Sizeof(Args{}))  //16
	fmt.Println(unsafe.Sizeof(Flag{}))  //8
}
