package main

import (
	"github.com/gin-gonic/gin"
	"message/util"
	"net/http"
)

func main()  {
	//初始化数据库连接
	db := util.InitDB()
	//获取静态文件路径
	staticPath := http.FileServer(http.Dir(util.ToolsApi.GetCurrentDirectory() + "static"))
	//设置静态资源访问路径
	http.Handle("/public/static/", http.StripPrefix("/public/static/",staticPath))
	r := gin.Default()
	r.LoadHTMLGlob("public/view/*.html")
	r.StaticFS("/static",http.Dir("public/static"))
	//定义路由
	var article []util.Article
	db.Find(&article)

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{"List": article})
	})
	r.Run()
}
