package message

import (
	"github.com/jinzhu/gorm"
	"html/template"
	"log"
	"message/util"
	"net/http"
)

var Message = new(messageApi)
type messageApi struct {

}

//首页控制器
func  (m *messageApi) Index(w http.ResponseWriter,r *http.Request)  {
	r.ParseForm()
	sort := r.FormValue("sort")
	if sort == "" {
		sort = "0"
	}
	//获取文章
	var data *util.Article
	//err := util.ToolsApi.JsonGet(fmt.Sprintf("https://www.dushu.com/news"),&data)
	var db *gorm.DB
	db.Table("msg_article").Scan(&data)
	if data == nil {
		log.Fatal("获取文章失败")
	}
	tpl,err :=template.ParseFiles("public/view/index")
	if err != nil {
		log.Fatal("解析失败")
	}
	err = tpl.Execute(w,tpl)
	log.Fatal(err)
}
