package util

type JsonListData struct {
	Sort int  `json:"sort"`
	List []Article `json:"list"`
}

type Article struct {
	Id      int    `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	ViewNum string `json:"viewnum"`
	CreateTime    int64  `json:"create_time"`
	UpdateTime   int64   `json:"update_time"`
}
