package util

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func InitDB() *gorm.DB  {
	db, err := gorm.Open("mysql", "root:root@(127.0.0.1:3306)/messagebox?charset=utf8mb4&parseTime=True&loc=Local")
	if err!= nil{
		panic(err)
	}
	return db
}
