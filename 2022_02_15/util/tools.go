package util

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)
var ToolsApi = new(toolsApi)
type toolsApi struct {

}
//获取当前的路径
func (t *toolsApi)GetCurrentDirectory() string {
	//返回绝对路径
	dir,err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(dir,"\\","/",-1)
}

func (t *toolsApi)JsonGet(url string, data interface{}) error {
	var content []byte
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	content, errIo := ioutil.ReadAll(response.Body)
	if errIo != nil {
		log.Fatal(errIo)
	}
	err = json.Unmarshal(content, data)
	return err
}
