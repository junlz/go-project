package main

import "fmt"



func getMedian(num1 []int, num2 []int) float64 {
	//先合并两个有序切片
	getArray := mergeSortAsc(num1,num2)
	medianLen := len(getArray)
	if medianLen % 2 == 0 {
		//中间第一个数
		leftMedian := medianLen / 2 - 1
		rightMedian := medianLen / 2
		medianNum := float64(getArray[leftMedian] + getArray[rightMedian]) / 2
		return medianNum
	}
	return float64(getArray[medianLen / 2])
}
//合并两个有序数组
func mergeSortAsc(num1,num2 []int) []int {
	n1,n2 :=  len(num1),len(num2)
	//创建一个新的切片存储新的切片,长度为0，容量为两个数组相加
	sortArray := make([]int, 0 , n1+n2)
	i,j := 0,0
	for i < n1 && j < n2 {
		switch  {
		case num1[i] < num2[j]:
			sortArray = append(sortArray,num1[i])
			i ++
		case num1[i] > num2[j]:
			sortArray = append(sortArray,num2[j])
			j ++
		default:
			sortArray = append(sortArray,num1[i],num2[j])
			i++
			j++
		}
	}
	if i < n1 {
		sortArray = append(sortArray, num1[i:]...) // 循环退出时 i 未被处理，需合并 [i:]
	}
	if j < n2 {
		sortArray = append(sortArray, num2[j:]...)
	}
	return sortArray
}

func main()  {
	num1 := []int{3,4,6,8}
	num2 := []int{2,5}
	newData := getMedian(num1,num2)
	fmt.Println(newData)
}

//输入：nums1 = [1,3], nums2 = [2]
//输出：2.00000
//解释：合并数组 = [1,2,3] ，中位数 2
//示例 2：
//
//输入：nums1 = [1,2], nums2 = [3,4]
//输出：2.50000
//解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5

//计算两个数组的中间值
