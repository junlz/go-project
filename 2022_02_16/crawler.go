package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	channelImageUrl chan string
	wg sync.WaitGroup
	chanTask chan string
	//匹配图片
	reImg = `https?://[^"]+?(\.((jpg)|(png)|(jpeg)|(gif)|(bmp)))`
)

func HandleError(err error, why string) {
	if err != nil {
		fmt.Println(why, err)
	}
}
//爬取图片
func main()  {
	//初始化管道
	channelImageUrl = make(chan string,1000000)
	chanTask = make(chan string,25)
	//启动多个爬虫协程
	for i:=1; i< 26; i++ {
		wg.Add(1)
		//go getImageUrls("https://www.bizhizu.cn/shouji/tag-%E5%8F%AF%E7%88%B1/" + strconv.Itoa(i) + ".html")
		//go getImageUrls("https://www.tupianzj.com/meinv/mm/list_218_" + strconv.Itoa(i) + ".html")
		go getImageUrls("http://www.netbian.com/shouji/index_" + strconv.Itoa(i) + ".html")
	}
	wg.Add(1)
	go CheckOK()
	//开启协程下载图片
	for i:=1; i<6; i++ {
		wg.Add(1)
		//下载图片直到完成
		go DownloadImage()
	}
	wg.Wait()

}
//下载图片(防止图片名称重复)
func DownloadImage()  {
	//从通道里读取信息
	for url := range channelImageUrl {
		fileName := GetFileNameFromUrl(url)
		//开始下载文件
		ok := DownloadFile(url,fileName)
		if ok {
			fmt.Printf("%s下载成功\n",fileName)
		}else{
			fmt.Printf("%s下载失败\n",fileName)
		}
	}
	wg.Done()
}

//开始下载文件
func DownloadFile(url string,filename string) (ok bool) {
	resp, err := http.Get(url)
	HandleError(err, "http.get.url")
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	HandleError(err, "resp.body")
	filename = "E:/picture/bizhi/" + filename
	// 写出数据
	err = ioutil.WriteFile(filename, bytes, 0666)
	if err != nil {
		return false
	} else {
		return true
	}
}

//获取图片的url地址
func GetFileNameFromUrl(url string) (filename string) {
	// 返回最后一个/的位置
	lastIndex := strings.LastIndex(url, "/")
	// 切出来
	filename = url[lastIndex+1:]
	// 时间戳解决重名
	timePrefix := strconv.Itoa(int(time.Now().UnixNano()))
	filename = timePrefix + "_" + filename
	return
}

// 任务统计协程
func CheckOK() {
	var count int
	for {
		url := <-chanTask
		fmt.Printf("%s 完成了爬取任务\n", url)
		count++
		if count == 25 {
			close(channelImageUrl)
			break
		}
	}
	wg.Done()
}

//获取图片的url

func getImageUrls(url string)  {
	//根据url获取当前页图片链接
	urls := getImage(url)
	//向通道发送数据
	for _,url := range urls {
		channelImageUrl <- url
	}
	//有多少任务发送多少条
	chanTask <- url
	wg.Done()
}

func getImage(url string) (urls []string) {
	pageStr := GetPageStr(url)
	re := regexp.MustCompile(reImg)
	results := re.FindAllStringSubmatch(pageStr, -1)
	fmt.Printf("共找到%d条结果\n", len(results))
	for _, result := range results {
		url := result[0]
		urls = append(urls, url)
	}
	return
}

// 抽取根据url获取内容
func GetPageStr(url string) (pageStr string) {
	resp, err := http.Get(url)
	HandleError(err, "http.Get url")
	defer resp.Body.Close()
	// 2.读取页面内容
	pageBytes, err := ioutil.ReadAll(resp.Body)
	HandleError(err, "ioutil.ReadAll")
	// 字节转字符串
	pageStr = string(pageBytes)
	return pageStr
}

