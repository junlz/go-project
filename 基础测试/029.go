package main

import (
	"fmt"
	"sync"
)

//交替打印

//打印基数
func oddNum(wg *sync.WaitGroup,ch chan int,num int)  {
	defer wg.Done()
	for i := 0; i <= num; i ++ {
		ch <- i
		if i % 2 != 0 {
			fmt.Println("基数:",i)
		}
	}
}

//打印偶数
func even(wg *sync.WaitGroup,ch chan int,num int)  {
	defer wg.Done()
	for i := 1; i <= num; i ++ {
		<- ch
		if i % 2 == 0 {
			fmt.Println("偶数:",i)
		}
	}
}

func main()  {
	wg := sync.WaitGroup{}
	cha := make(chan int)
	wg.Add(1)
	go oddNum(&wg,cha,10)
	go even(&wg,cha,10)
	wg.Wait()
}

