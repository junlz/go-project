package main

import "fmt"

//给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
//
//示例 1：
//
//输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
//输出：6
//解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。

func maxSubArray(nums []int) int {
	//找出最大数
	max := 0
	//定义最大的数
	maxSum,sums := 0,0
	for _,n := range nums {
		if max < n {
			max = n
		}
	}
	//开始循环
	for i :=1; i < len(nums); i++{
		switch  {
		case nums[i] > 0:
			sums += nums[i]
		case nums[i] < 0:
			if sums + nums[i] > 0 {
				sums += nums[i]
			}else{
				sums = 0
			}
		}
		if maxSum < sums {
			maxSum = sums
		}
	}

	if maxSum > max && max < 0{
		return max
	}
	return  maxSum

}

func main()  {
	fmt.Println(maxSubArray([]int{2,-1,4,2,-9,8}))
}