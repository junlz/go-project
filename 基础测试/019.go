package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(MissingNumber([]int{3, 0, 1}))
	fmt.Println(BestMissingNumber([]int{3, 0, 1}))
}

func BestMissingNumber(nums []int) int {
	curSum := 0
	for i := range nums {
		curSum += nums[i]
	}
	n := len(nums)
	fullSum := n * (n + 1) / 2
	return fullSum - curSum
}

// 辣鸡解法
func MissingNumber(nums []int) int {
	sort.Ints(nums) // lgN
	for i := 0; i < len(nums); i++ { // N
		if nums[i] != i {
			return i
		}
	}
	return len(nums)
}