package main

import (
	"errors"
	"time"
)

func (s *passRechargeServer) ChannelCheck(req *define.CheckStatus) ( err error) {
	tx, err := dao.TransferChannel.DB.Begin()
	var transferStatus,payStatus,status = 0,0,0
	//实际汇款金额
	var errs error
	realAmount := req.Amount
	defer func() {
		if errs != nil {
			err = errs
			return
		}
		if err != nil {
			_ = tx.Rollback()
		} else {
			_ = tx.Commit()
		}
	}()
	defer func() {
		tempUpdata := g.Map{
			"status":status,
			"real_recharge_amount":realAmount / 100,
			"transfer_status":transferStatus,
			"recharge_time": gconv.Int(time.Now().Unix()),
			"remark":req.Remark,
			"pay_status" :payStatus,
			"updated_time":gconv.Int(time.Now().Unix()),
			"reviewer" :req.Uid,
		}
		_,err = dao.TransferChannel.WherePri(req.Id).Update(tempUpdata)
		if err != nil {
			err = code.CreateSysErr(err)
		}
	}()
	checkData,err := dao.TransferChannel.Fields("id,pay_status,amount,appid,uid,channel_id").One(dao.TransferChannel.Columns.Id,req.Id)
	if err != nil {
		errs = errors.New("该订单不存在")
		return
	}
	checkMap := gconv.Map(checkData)
	appId := gconv.Int(gconv.Map(checkMap)["appid"])
	uid := gconv.Int(gconv.Map(checkMap)["uid"])
	accountlInfo, err := s.GetUserInfo(uid)
	if err != nil {
		errs = errors.New("用户信息不存在")
		return
	}
	channelId := gconv.Int(gconv.Map(accountlInfo)["copartner_id"])
	//同意转账后渠道加款改状态
	if req.Status == 1 {
		//调用扣款接口
		err = shared.Channel.PassAddBalance(tx, channelId, uid, appId, uint(realAmount), 8, " 采购预付款")
		if nil != err {
			err = code.CreateSysErr(err)
			return
		}
		payStatus = 1
		transferStatus = 1
	}else{
		realAmount = 0
	}
	return
}
