package main

import "fmt"

//比较两个切面的大小

//func stringSliceEqal(a,b []int) bool {
//	if len(a) != len(b) {
//		return false
//	}
//	if (a == nil) != (b == nil) {
//		return false
//	}
//	//b = b[:len(a)]
//	//fmt.Printf()
//	for i,v := range a {
//		if v != b[i] {
//			return false
//		}
//	}
//	return true
//}

type Stu struct {
	Name string
}

func main() {
	fmt.Printf("%v\n", Stu{"Tom"}) // {Tom}
	fmt.Printf("%+v\n", Stu{"Tom"}) // {Name:Tom}
}
