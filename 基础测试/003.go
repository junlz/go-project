package main

import "fmt"

//输入: [1,3,5,6], 7
//输出: 4

func searchIndexs(arr []int, num int) (indexs int) {
	var index = 0
	for k,v := range arr {
		if v == num {
			index = k
		}
		if arr[k] < num && arr[k+1] > num{
			arr[k+1] = num
			index = k + 1
		}
	}
	return index
}

func main()  {
	s := []int{1,2,5,7,9}
	num := 5
	fmt.Println(searchIndexs(s,num))
}