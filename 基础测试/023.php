<?php

//定义二叉树的类
class Node {
    public $data;
    public $left = null;
    public $right = null;
    public function __construct($data)
    {
        $this->data = $data;
    }
}
/***
 * 二叉树的增和查询方法
 */
class BinarySortTree {
    //获取当前树
    private $tree;
    public function getTree()
    {
        return $this->tree;
    }
    //插入元素
    /**
     * @param mixed $tree
     */
    public function insertTree($data)
    {
        //如果树不存在节点就为跟节点
        if(!$this->tree) {
            $this->tree = new Node($data);
            return;
        }
        $p = $this->tree;
        while ($p) {
            //如果小于就为左节点
            if ($data < $p->data){
                if (!$p->left){
                    $p->left = new Node($data);
                    return;
                }
                $p = $p ->left;
            }elseif ($data > $p -> data) {
                if (!$p->right){
                    $p->right = new Node($data);
                    return;
                }
                $p = $p->right;
            }
        }

    }
    /***
     * 二叉树查找
     */
    public function findTreeNode($data)
    {
        $p = $this->tree;
        while ($p) {
             if ($data < $p->data){
                 $p = $p ->left;
             }elseif ($data > $p->data){
                 $p = $p ->right;
             } else{
                 return $p;
             }
        }

    }
}

$tree = new BinarySortTree();
$tree->insertTree(5);
$tree->insertTree(8);
$tree->insertTree(4);
$tree->insertTree(1);
$tree->insertTree(9);
$tree->insertTree(2);
$tree->insertTree(7);
$tempData = $tree->getTree();
$findData = $tree->findTreeNode(4);
print_r($findData);