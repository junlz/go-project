package main

import "fmt"

// 存放文章信息的 Post 结构体
type Post struct {
	Id int
	Title string
	Content string
	Author string
}

// 以 ID 字段为键的 Post 字典
var PostsById map[int]*Post
// 以作者字段为键的 Post 切片字典（一个作者可能对应多篇文章）
var PostsByAuthor map[string][]*Post

// 添加 Post 实例到对应的字典
func store(post Post) {
	PostsById[post.Id] = &post
	PostsByAuthor[post.Author] = append(PostsByAuthor[post.Author], &post)
}

func main()  {
	// 初始化字典
	PostsById = make(map[int]*Post)
	PostsByAuthor = make(map[string][]*Post)

	// 初始化文章信息
	post1 := Post{Id: 1, Title: "PHP 全栈工程师指南", Content: "基于 Laravel + Vue.js 开发 Web 项目", Author: "学院君"}
	post2 := Post{Id: 2, Title: "Go 入门教程", Content: "Go 语言基础语法和使用指南", Author: "学院君"}
	post3 := Post{Id: 3, Title: "Go Web 编程", Content: "基于 Gin 框架构建 Web 项目", Author: "学院君"}
	post4 := Post{Id: 4, Title: "微服务从入门到实践", Content: "基于 Laravel + go-micro 框架构建微服务", Author: "学院君"}

	// 存储文章到字典
	store(post1)
	store(post2)
	store(post3)
	store(post4)

	// 测试1：打印特定文章（从字典通过 ID 获取文章）
	fmt.Println(PostsById[1])
	fmt.Println(PostsById[2])

	// 测试2：打印特定文章（从字典通过作者获取文章切片）
	for _, post := range PostsByAuthor["学院君"] {
		fmt.Println(post)
	}
}