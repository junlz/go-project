package main

import "fmt"

func main()  {
	doule := adds()
	fmt.Println(doule())
	fmt.Println(doule())
	fmt.Println(doule())
	fmt.Println(doule())
}

func adds() func() int {
	var r int
	return func() int {
		r++
		return r*2
	}
}
