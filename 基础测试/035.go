package main

import (
	"fmt"
	"time"
)

func main() {
	//    子协程：写10个数据，每写一次，就阻塞一次，主协程读取一次，阻塞解除一次
	//    主协程：读取一次数据，阻塞一次，子协程写一次，阻塞解除一次
	ch1 := make(chan int)
	go sendData(ch1)
	//读取chanel中的数据，不过由于我们可能不知道发送方发送了多少数据， 这里用死循环
	for {
		time.Sleep(1 * time.Second)
		v, ok := <-ch1
		if !ok {
			fmt.Println("all the data has bean read ", ok)
			break
		}
		fmt.Println("the data read is: ", v, ok)
	}
}

func sendData(ch1 chan int) {
	// 发送方： 发送10条数据
	for i := 0; i < 10; i++ {
		//把i写入chanel
		ch1 <- i
	}
	//关闭ch1通道
	close(ch1)
}
