package main

import "fmt"

// map 错误示例
//func main() {
//	var m map[string]int
//	m["one"] = 1  // error: panic: assignment to entry in nil map
//	// m := make(map[string]int)// map 的正确声明，分配了实际的内存
//}

//// slice 正确示例
//func main() {
//	var s []int
//	s = append(s, 1)
//	fmt.Println(s)
//}
//当访问 map 中不存在的 key 时，Go 则会返回元素对应数据类型的零值，比如 nil、’’ 、false 和 0，取值操作总有值返回，故不能通过取出来的值，来判断 key 是不是在 map 中。
//
//检查 key 是否存在可以用 map 直接访问，检查返回的第二个参数即可。

func main() {
	x := map[string]string{"one": "2", "two": "", "three": "3"}
	if _,ok := x["two"]; ok {
		fmt.Println("key two is no entry")
	}
}
