<?php

class Node {
    public  $root;
    public $left = null;
    public $right = null;
    //初始化
    public function __construct($root)
    {
        $this->root = $root;
    }

    /**
     * @return mixed
     * 前序遍历
     */
    public function getRoot($tree)
    {
        if ($tree == null) {
            return;
        }
        printf("%s\n",$tree->root);
        $this->getRoot($tree->left);
        $this->getRoot($tree->right);
    }

    /**
     * @return null
     * 中序遍历
     */
    public function getLeft($tree)
    {
        if ($tree == null) {
            return;
        }
        $this->getRoot($tree->left);
        printf("%s\n",$tree->root);
        $this->getRoot($tree->right);
    }

    /**
     * @param null $right
     * 后序遍历
     */
    public function getRight($tree)
    {
        if ($tree == null) {
            return;
        }
        $this->getRoot($tree->left);
        $this->getRoot($tree->right);
        printf("%s\n",$tree->root);
    }

}


$node1 = new Node('a');
$node2 = new Node('b');
$node3 = new Node('c');
$node1 ->left = $node2;
$node1->right = $node3;


$node1->getRoot($node1);
print "===========\n";
$node1->getLeft($node1);
print "===========\n";
$node1->getRight($node1);
print "===========\n";
