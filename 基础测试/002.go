package main

import (
	"encoding/json"
	"fmt"
	"log"
)

//string 类型的值可以修改吗

// 修改字符串的错误示例
//func main() {
//	x := "text"
//	x[0] = "T"  // error: cannot assign to x[0]
//	fmt.Println(x)
//}


// 修改示例
func main() {
	//x := "text"
	//xBytes := []byte(x)
	//xBytes[0] = 'T' // 注意此时的 T 是 rune 类型
	//xBytes[1] = 'U'
	//x = string(xBytes)
	//fmt.Println(x) // Text
	var data = []byte(`{"status": 200}`)
	var result map[string]interface{}

	if err := json.Unmarshal(data, &result); err != nil {
		log.Fatalln(err)

	}
	fmt.Println(result)
}
