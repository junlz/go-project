package main

import (
	"context"
	"fmt"
	"sync"
)

//协程中断的例子,当协程执行超过5次以后中断协程
func main()  {
	//初始化一个context
	parent := context.Background()
	//生成一个取消context
	ctx,cancle := context.WithCancel(parent)
	var wg sync.WaitGroup
	runTime := 0
	wg.Add(1)
	go func(ctx context.Context) {
		//创建阻塞
		for {
			select {
				case <- ctx.Done():
				fmt.Println("The goruntine is stop")
					return
			default:
				fmt.Printf("The goruntine running time: %d\n",runTime)
				runTime += 1
			}
			if runTime > 5 {
				cancle()
				wg.Done()
			}
		}
	}(ctx)
	wg.Wait()



}
