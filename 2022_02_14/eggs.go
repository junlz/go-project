package main

import (
	"fmt"
	"sync"
)

func main()  {
	//100个小伙抢10个鸡蛋
	eggs := make(chan int,10)
	for i:=0; i< 10; i++ {
		eggs <- i
	}
	//初始化协程组
	var wg sync.WaitGroup
	//100个小伙伴(100个协程)
	for i:=0; i < 100; i ++ {
		wg.Add(1)
		go func(num int) {
			select {
			case egg := <-eggs:
				fmt.Printf("第 %d位小伙伴抢到鸡蛋的编号为: %d\n",num,egg)
			default:
			}
			wg.Done()
		}(i)
	}
	wg.Wait()
	//第 0位小伙伴抢到鸡蛋的编号为: 0
	//第 99位小伙伴抢到鸡蛋的编号为: 1
	//第 1位小伙伴抢到鸡蛋的编号为: 2
	//第 2位小伙伴抢到鸡蛋的编号为: 3
	//第 3位小伙伴抢到鸡蛋的编号为: 4
	//第 4位小伙伴抢到鸡蛋的编号为: 5
	//第 5位小伙伴抢到鸡蛋的编号为: 6
	//第 6位小伙伴抢到鸡蛋的编号为: 7
	//第 53位小伙伴抢到鸡蛋的编号为: 8
	//第 54位小伙伴抢到鸡蛋的编号为: 9

}
